package queue

type Queue[T any] struct {
	// this is not the greatest queue - we are constantly
	// throwing out and re-allocating buffers. but it's
	// good enough for aoc.
	q []T
}

func (q *Queue[T]) Enqueue(v T) {
	q.q = append(q.q, v)
}

func (q *Queue[T]) Dequeue() (T, bool) {
	if len(q.q) == 0 {
		var z T
		return z, false
	}
	v := q.q[0]
	q.q = q.q[1:]
	return v, true
}
