package main

import (
	"aoc/util"
	"fmt"
	"strings"
)

func main() {
	scanner, close := util.Input()
	defer close()

	// a note on coordinates: the point 0,0 is the upper-left
	// of our 2d space. We refer to our coordinates as x,y, and
	// as indices into our grid this is g.c[x][y]. 'x' increases
	// as we move to the right, and 'y' increases as we move down.

	var g grid
	var line int
	for scanner.Scan() {
		line++
		str := scanner.Text()
		pieces := strings.Split(str, " -> ")

		var segment []point
		for _, piece := range pieces {
			var p point
			_, err := fmt.Sscanf(piece, "%d,%d", &p.x, &p.y)
			if err != nil {
				panic(fmt.Sprintf("error parsing point %q on line %d: %s", piece, line, err))
			}
			segment = append(segment, p)
		}

		// take the segments pairwise, and draw them
		for i := 1; i < len(segment); i++ {
			g.draw(segment[i-1], segment[i])
		}
	}
	count := g.simulate()
	fmt.Println("count: ", count)
}

type point struct {
	x, y int
}

type grid struct {
	c      [][]byte
	maxRow int
	floor  int
}

// draw fills in fixed-barriers within the grid. The points
// from and to must for a horizontal or vertical line.
func (g *grid) draw(from, to point) {
	g.grow(from.x, from.y)
	g.grow(to.x, to.y)

	switch {
	case from.x == to.x:
		if from.y > to.y {
			from.y, to.y = to.y, from.y
		}
		for i := from.y; i <= to.y; i++ {
			g.c[from.x][i] = 1
		}
	case from.y == to.y:
		if from.x >= to.x {
			from.x, to.x = to.x, from.x
		}
		for i := from.x; i <= to.x; i++ {
			g.c[i][from.y] = 1
		}
	default:
		panic(fmt.Sprintf("Unexpected draw: from %#v to %#v", from, to))
	}
	if from.y > g.maxRow {
		g.maxRow = from.y
		g.floor = g.maxRow + 2
	}
	if to.y > g.maxRow {
		g.maxRow = to.y
		g.floor = g.maxRow + 1
	}
}

// grow expands the size of the grid to include the given point
func (g *grid) grow(x, y int) {
	if len(g.c) == 0 {
		g.c = append(g.c, make([]byte, y+1))
	}

	todo := y + 1 - len(g.c[0])
	if todo > 0 {
		for i := 0; i < len(g.c); i++ {
			g.c[i] = append(g.c[i], make([]byte, todo)...)
		}
	}

	todo = x + 1 - len(g.c)
	for todo > 0 {
		todo--
		g.c = append(g.c, make([]byte, len(g.c[0])))
	}
}

func (g *grid) simulate() int {
	// Drop sand from 500,0 one grain at a time
	// until we reach steady-state (that is, the dropped
	// grain does not rest on a barrier placed by 'draw').
	//
	// We can test for this if a grain reaches a cell lower
	// than the lowest drawn barrier.
	//
	// The return value is the simulation iteration at which
	// we observe this condition.

	// make sure we have enough room for the floor
	g.grow(500, g.floor+1)
	// make sure we have enough room to spill sideways
	g.grow(1000, 0)

	var atRest int
	for {
		x, y := 500, 0
	oneGrain:
		for {
			switch {
			case g.c[x][y+1] == 0 && y < g.floor:
				// we can move down
				y++
				continue
			case g.c[x-1][y+1] == 0 && y < g.floor:
				// we can move to the lower-left
				x--
				y++
				continue
			case g.c[x+1][y+1] == 0 && y < g.floor:
				// we can move to the lower-right
				x++
				y++
				continue
			default:
				// we come to rest
				atRest++
				g.c[x][y] = 2

				// did we come to rest at the source?
				if x == 500 && y == 0 {
					return atRest
				}
				break oneGrain
			}
		}
	}
}
