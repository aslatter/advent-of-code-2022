package util

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func Input() (*bufio.Scanner, func() error) {
	var inputPath string

	flag.StringVar(&inputPath, "input", "", "path to input file")
	flag.Parse()

	if inputPath == "" {
		fmt.Println("flag --input is required")
		os.Exit(1)
	}

	file, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}
	scanner := bufio.NewScanner(file)
	return scanner, file.Close
}
