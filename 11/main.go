package main

import (
	"aoc/util"
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/exp/slices"
)

func main() {
	scanner, close := util.Input()
	defer close()

	var state []monkey

	// parse
	for {
		var m monkey

		var lines [6]string
		for i := 0; i < len(lines); i++ {
			if !scanner.Scan() {
				panic("unexpected EOF on monkey " + strconv.Itoa(i))
			}
			lines[i] = strings.TrimSpace(scanner.Text())
		}

		/** Example input:
				0: Monkey 0:
				1:	Starting items: 79, 98
		  		2:	Operation: new = old * 19
				3:	Test: divisible by 23
				4:		If true: throw to monkey 2
				5:		If false: throw to monkey 3
				**/

		// line 0 - not interesting

		m.parseItems(lines[1])
		m.parseOperation(lines[2])
		m.parseTest(lines[3])
		m.parseTrueCond(lines[4])
		m.parseFalseCond(lines[5])

		// done with parsing
		state = append(state, m)

		// consume whitespace (or be done)
		if !scanner.Scan() {
			break
		}
	}

	// our numbers will get too big if we can't somehow keep
	// them small. We find a modulus that we can do all of
	// our math relative to while still preserving the meaning.
	var modulus int
	for i := range state {
		m := &state[i]
		if modulus == 0 {
			modulus = m.test
			continue
		}
		// we could break everything down to prime-factors, so as
		// to not pull in the same factors multiple times, but I'm
		// too lazy for that (and this is good enough).
		if modulus%m.test != 0 {
			modulus *= m.test
		}
	}

	// run
	for i := 0; i < 10000; i++ {
		for i := range state {
			m := &state[i]
			for {
				item, ok := m.nextItem()
				if !ok {
					break
				}

				// consider
				item = m.op.eval(item)
				m.counter++

				// reduce
				item %= modulus

				// throw
				destIx := 0
				if item%m.test != 0 {
					destIx = 1
				}
				state[m.dest[destIx]].receiveItem(item)
			}
		}

		// debugging
		switch i + 1 {
		case 1, 20, 1_000, 2_000:
			for j, m := range state {
				fmt.Println(j, ": ", m.counter)
			}
			fmt.Println()
		}
	}

	// find two busiest monkeys
	slices.SortFunc(state, func(l, r monkey) bool {
		return r.counter < l.counter
	})

	// find the product of their inspections
	fmt.Println("monkey business total: ", state[0].counter*state[1].counter)
}

type monkey struct {
	items   []int
	op      operation
	test    int
	dest    [2]int
	counter int
}

func (m *monkey) nextItem() (int, bool) {
	if len(m.items) == 0 {
		return 0, false
	}
	// this might leak memory if we run too long?
	item := m.items[0]
	m.items = m.items[1:]
	return item, true
}

func (m *monkey) receiveItem(item int) {
	m.items = append(m.items, item)
}

func (m *monkey) parseItems(line string) {
	if !strings.HasPrefix(line, "Starting items:") {
		panic(fmt.Sprintf("malformed items string %q", line))
	}
	str := strings.TrimPrefix(line, "Starting items:")

	pieces := strings.Split(str, ",")
	for i, pc := range pieces {
		pc := strings.TrimSpace(pc)
		n, err := strconv.Atoi(pc)
		if err != nil {
			panic(fmt.Sprintf("error parsing number %d in %q: %s", i, str, err))
		}
		m.receiveItem(n)
	}
}

func (m *monkey) parseOperation(line string) {
	var opStr string
	var argStr string
	_, err := fmt.Sscanf(line, "Operation: new = old %s %s", &opStr, &argStr)
	if err != nil {
		panic(fmt.Sprintf("error parsing operation from %q: %s", line, err))
	}

	if argStr == "old" {
		m.op.typ = operationTypeSquare
		return
	}

	m.op.arg, err = strconv.Atoi(argStr)
	if err != nil {
		panic(fmt.Sprintf("error parsing argument %q from %q: %s", argStr, line, err))
	}

	switch opStr {
	case "*":
		m.op.typ = operationTypeMult
	case "+":
		m.op.typ = operationTypeAdd
	default:
		panic(fmt.Sprintf("unknown operand %q in operations %q", opStr, line))
	}
}

func (m *monkey) parseTest(line string) {
	_, err := fmt.Sscanf(line, "Test: divisible by %d", &m.test)
	if err != nil {
		panic(fmt.Sprintf("error parsing test from %q: %s", line, err))
	}
}

func (m *monkey) parseTrueCond(line string) {
	_, err := fmt.Sscanf(line, "If true: throw to monkey %d", &m.dest[0])
	if err != nil {
		panic(fmt.Sprintf("error parsing condition from %q: %s", line, err))
	}
}

func (m *monkey) parseFalseCond(line string) {
	_, err := fmt.Sscanf(line, "If false: throw to monkey %d", &m.dest[1])
	if err != nil {
		panic(fmt.Sprintf("error parsing condition from %q: %s", line, err))
	}
}

const (
	operationTypeAdd = iota + 1
	operationTypeMult
	operationTypeSquare
)

type operation struct {
	typ int
	arg int
}

func (o operation) eval(input int) int {
	switch o.typ {
	case operationTypeAdd:
		return input + o.arg
	case operationTypeMult:
		return input * o.arg
	case operationTypeSquare:
		return input * input
	default:
		panic(fmt.Sprintf("unknown operation: %v", o.typ))
	}
}
