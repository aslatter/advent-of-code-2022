package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	var inputPath string
	flag.StringVar(&inputPath, "input", "", "path to input file")
	flag.Parse()

	if inputPath == "" {
		fmt.Print("flag --input is required")
		os.Exit(1)
	}

	file, err := os.Open(inputPath)
	if err != nil {
		fmt.Printf("opening file %q: %s", inputPath, err)
		os.Exit(1)
	}

	var score int

	r := bufio.NewScanner(file)
	for r.Scan() {
		line := strings.TrimSpace(r.Text())
		if line == "" {
			continue
		}

		pieces := strings.SplitN(line, " ", 3)
		if len(pieces) != 2 {
			fmt.Printf("unexpected line: %q", line)
			os.Exit(1)
		}

		score += calcScore(pieces[0], pieces[1])
	}

	fmt.Println("Score: ", score)
}

func calcScore(col1, col2 string) (score int) {
	switch col2 {
	case "X":
		// lose
		switch col1 {
		case "A":
			// rock
			score += 3
		case "B":
			score += 1
		case "C":
			score += 2
		}
	case "Y":
		// draw
		score += 3
		switch col1 {
		case "A":
			// rock
			score += 1
		case "B":
			// paper
			score += 2
		case "C":
			// scissors
			score += 3
		}
	case "Z":
		// win
		score += 6
		switch col1 {
		case "A":
			// rock
			score += 2
		case "B":
			// paper
			score += 3
		case "C":
			// scissors
			score += 1
		}
	}

	return
}
