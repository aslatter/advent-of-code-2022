package main

import (
	"aoc/util"
	"flag"
	"fmt"

	"golang.org/x/exp/slices"
)

func main() {
	var limit int
	flag.IntVar(&limit, "limit", 20, "rows to check")

	scanner, close := util.Input()
	defer close()

	state := make([]rangeSet, limit+1)

	for scanner.Scan() {
		sensorStr := scanner.Text()
		// Sensor at x=2, y=18: closest beacon is at x=-2, y=15
		var sensorX, sensorY, beaconX, beaconY int
		fmt.Sscanf(sensorStr,
			"Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d",
			&sensorX, &sensorY, &beaconX, &beaconY)

		distance := abs(sensorX-beaconX) + abs(sensorY-beaconY)

		for row := 0; row < len(state); row++ {
			remainder := distance - abs(sensorY-row)
			if remainder < 0 {
				continue
			}

			state[row].add(sensorX-remainder, sensorX+remainder)
		}
	}

	fmt.Println("finished scanning ...")

	// find open position < limit
	var foundX, foundY int
	for row := 0; row < len(state); row++ {
		if len(state[row].s) == 0 {
			continue
		}
		p := &state[row].s[0]
		if p.begin > 0 {
			foundX, foundY = 0, row
			break
		}
		if p.end < limit {
			foundX, foundY = p.end+1, row
			break
		}
	}

	fmt.Println("missing: ", foundX, ",", foundY)
	fmt.Println("score: ", foundX*4_000_000+foundY)
}

// rangeSet stores contiguous ranges of integers
type rangeSet struct {
	// ideally this would be a binary tree, but because
	// I'm lazy it's a sorted array. This is fast enough
	// (and it could be optimized further)
	s []r
}

// add adds a contiguous range of integers to the set
func (s *rangeSet) add(begin, end int) {
	pos, found := slices.BinarySearchFunc(s.s, r{begin, end}, func(l, r r) int {
		return l.begin - r.begin
	})
	if found {
		s.extendExisting(pos, end)
		return
	}
	// can we extend the previous range?
	if pos > 0 && begin <= s.s[pos-1].end+1 {
		s.extendExisting(pos-1, end)
		return
	}
	// can we extend the next range?
	if pos < len(s.s) && s.s[pos].begin <= end+1 {
		cur := &s.s[pos]
		if begin < cur.begin {
			cur.begin = begin
		}
		if cur.end < end {
			cur.end = end
			s.maybeCoalesce(pos)
		}
		return
	}
	// insert
	s.s = slices.Insert(s.s, pos, r{begin, end})
}

// extendExisting modifies the range at pos to be at
// least as long as end.
func (s *rangeSet) extendExisting(pos, end int) {
	if end <= s.s[pos].end {
		return
	}
	s.s[pos].end = end
	s.maybeCoalesce(pos)
}

// maybeCoalesce checks to see if the range at
// pos overlaps with any other ranges, and if so
// combines them.
func (s *rangeSet) maybeCoalesce(pos int) {
	if len(s.s) <= pos+1 {
		return
	}

	// because we sort our array based on the
	// start of a range, we never have to check
	// "to the left" of our range for coalescing.

	cur := &s.s[pos]
	var i int
	for i = pos + 1; i < len(s.s); i++ {
		next := &s.s[i]
		if cur.end+1 < next.begin {
			break
		}
		if next.end > cur.end {
			cur.end = next.end
		}
	}
	s.s = slices.Delete(s.s, pos+1, i)
}

type r struct {
	begin, end int
}

func abs(v int) int {
	if v < 0 {
		return -v
	}
	return v
}
