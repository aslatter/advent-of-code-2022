package main

import "testing"

func TestRangeSet(t *testing.T) {
	t.Run("join next", func(t *testing.T) {
		var rs rangeSet
		rs.add(4, 5)
		rs.add(1, 3)
		if len(rs.s) != 1 {
			t.Fatalf("expected set to have 1 segment, got %d", len(rs.s))
		}
	})
}
