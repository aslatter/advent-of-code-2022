package main

import (
	"aoc/util"
	"fmt"
	"strings"
)

func main() {
	scanner, close := util.Input()
	defer close()

	var bins [][]byte

	// parse initial state of bins
	for scanner.Scan() {
		str := scanner.Text()
		if str == "" {
			// no more inputs
			break
		}

		// process each open-brace
		nextBrace := nextIndexByte(str, '[', 0)
		for ; nextBrace != -1; nextBrace = nextIndexByte(str, '[', nextBrace+1) {
			label := str[nextBrace+1]
			index := nextBrace / 4

			if index > len(bins)-1 {
				newBins := make([][]byte, index+1)
				copy(newBins, bins)
				bins = newBins

			}

			bins[index] = append(bins[index], label)
		}
	}

	// we need to reverse the order of the crates in our bins
	// (because the ascii art starts from the top ...)
	for _, bin := range bins {
		for i, j := 0, len(bin)-1; i < j; i, j = i+1, j-1 {
			bin[i], bin[j] = bin[j], bin[i]
		}
	}

	dump(bins)

	// process instructions
	for scanner.Scan() {
		str := scanner.Text()
		if str == "" {
			// ??
			continue
		}
		ins := parseInstruction(str)

		fromSlice := bins[ins.from-1]
		toSlice := bins[ins.to-1]

		l := len(fromSlice)
		taken := fromSlice[l-ins.move : l]

		fromSlice = fromSlice[:l-ins.move]
		toSlice = append(toSlice, taken...)

		bins[ins.from-1] = fromSlice
		bins[ins.to-1] = toSlice

		dump(bins)
	}

	// read out result
	var sb strings.Builder
	for _, bin := range bins {
		b := bin[len(bin)-1]
		sb.WriteRune(rune(b))
	}
	fmt.Println(sb.String())
}

func nextIndexByte(s string, b byte, from int) int {
	p := strings.IndexByte(s[from:], b)
	if p == -1 {
		return p
	}
	return from + p
}

type instruction struct {
	move int
	from int
	to   int
}

func parseInstruction(s string) (i instruction) {
	// move 3 from 1 to 3
	_, err := fmt.Sscanf(s, "move %d from %d to %d", &i.move, &i.from, &i.to)
	if err != nil {
		panic(err)
	}
	return
}

func dump(bins [][]byte) {
	for i, bin := range bins {
		fmt.Printf("%d - ", i)
		for _, b := range bin {
			fmt.Printf("%s", string([]byte{b}))
		}
		fmt.Println()
	}
	fmt.Println()
}
