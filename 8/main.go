package main

import (
	"aoc/util"
	"fmt"
)

func main() {
	sc, close := util.Input()
	defer close()

	var s state

	// we build a grid of heights, and then loop
	// over the grid keeping track of how far away
	// we've seen different-height trees. This is
	// not much state - we only have ten different
	// possible tree-heights.
	//
	// Then per tree, we can use this lookup table
	// to determine how many trees we can see from
	// that spot.
	//
	// We need to do this "from" each direction.
	//
	// Because we build the grid from upper-left to
	// lower-right, we can handle two of these
	// directions while we fill in the grid.
	//
	// Then if we sweep from the lower-right to upper
	// left we can do the other two directions (and
	// get the final visibility per tree as we go).

	// maximum heights seen per column
	var distances []heightDistance
	for sc.Scan() {
		line := sc.Bytes()

		// maximum height seen in this row
		var distance heightDistance

		if len(distances) == 0 {
			distances = make([]heightDistance, len(line))
		}
		gline := make([]tree, len(line))

		for i, h := range line {
			// convert ascii code to decimal height
			h = h - '0'

			gline[i].h = h

			gline[i].lScore = distance.score(h)
			distance.increment(h)

			gline[i].tScore = distances[i].score(h)
			distances[i].increment(h)
		}
		s.grid = append(s.grid, gline)

	}
	// now, sweep from the bottom-left to top-right
	// propagating heights as we go
	var maxVisibility int
	distances = make([]heightDistance, len(s.grid))
	for i := len(s.grid) - 1; i >= 0; i-- {
		gline := s.grid[i]
		var maxHeight heightDistance
		for j := len(gline) - 1; j >= 0; j-- {
			h := gline[j].h

			gline[j].rScore = maxHeight.score(h)
			maxHeight.increment(h)

			gline[j].bScore = distances[j].score(h)
			distances[j].increment(h)

			visibility := gline[j].score()
			if maxVisibility < visibility {
				maxVisibility = visibility
			}
		}
	}
	fmt.Println("max visibility: ", maxVisibility)
}

type state struct {
	grid [][]tree
}

type tree struct {
	h      byte
	lScore int
	rScore int
	tScore int
	bScore int
}

func (t *tree) score() int {
	return t.lScore * t.rScore * t.tScore * t.bScore
}

// heightDistance stores, for a given height, how
// far away we've seen that height.
type heightDistance [10]int

func (h *heightDistance) increment(current byte) {
	for i := byte(0); i < 10; i++ {
		if i > current {
			(*h)[i]++
		} else {
			// start at one (not zero) because
			// this tree is visible, and so is
			// worth some amount of scenery.
			(*h)[i] = 1
		}
	}
}

func (h *heightDistance) score(current byte) int {
	return (*h)[current]
}
