package main

import (
	"aoc/util"
	"fmt"
)

func main() {
	s, close := util.Input()
	defer close()

	var st state
	st.markPosition()

	for s.Scan() {
		str := s.Text()

		var d string
		var l int

		_, err := fmt.Sscanf(str, "%s %d", &d, &l)
		if err != nil {
			panic(err)
		}

		st.move(d, l)
	}

	fmt.Println("tail has visited: ", len(st.seen))
}

const ropeLen = 10

type state struct {
	x, y [ropeLen]int
	seen map[point]bool
}

type point struct {
	x, y int
}

// move moves the 'head' in direction d, l times. The tail
// is adjusted to follow the head.
func (s *state) move(d string, l int) {
	for i := 0; i < l; i++ {
		s.moveOnce(d)
	}
}

// moveOnce moves the head in the direction d. If needed
// the tail is adjusted to follow the head.
func (s *state) moveOnce(d string) {
	switch d {
	case "U":
		s.y[0]++
	case "D":
		s.y[0]--
	case "L":
		s.x[0]--
	case "R":
		s.x[0]++
	}
	s.moveTail()
}

// moveTail adjusts the position of the tail to follow
// the head (if needed), and marks the new position as
// visited.
func (s *state) moveTail() {
	for i := 1; i < ropeLen; i++ {
		xdiff := s.x[i-1] - s.x[i]
		ydiff := s.y[i-1] - s.y[i]

		switch {
		// if head and tail are touching we're done
		case abs(xdiff) < 2 && abs(ydiff) < 2:
			// noop
			return

		// difference of two in same
		// column/row results in movement same column/row
		case xdiff > 1 && ydiff == 0:
			s.x[i]++
		case xdiff < -1 && ydiff == 0:
			s.x[i]--
		case xdiff == 0 && ydiff > 1:
			s.y[i]++
		case xdiff == 0 && ydiff < -1:
			s.y[i]--

		// if we're in different
		// column & row, move diagonally "towards"
		// head
		case xdiff > 0 && ydiff > 0:
			s.x[i]++
			s.y[i]++
		case xdiff > 0 && ydiff < 0:
			s.x[i]++
			s.y[i]--
		case xdiff < 0 && ydiff < 0:
			s.x[i]--
			s.y[i]--
		case xdiff < 0 && ydiff > 0:
			s.x[i]--
			s.y[i]++
		}
	}
	s.markPosition()
}

// markPosition logs the current position of
// the tail as visited
func (s *state) markPosition() {
	if s.seen == nil {
		s.seen = make(map[point]bool)
	}

	var p point
	p.x = s.x[ropeLen-1]
	p.y = s.y[ropeLen-1]

	s.seen[p] = true
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}
