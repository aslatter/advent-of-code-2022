package main

import (
	"aoc/util"
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"golang.org/x/exp/slices"
)

func main() {
	scanner, close := util.Input()
	defer close()

	var packets []any
	for scanner.Scan() {
		l := scanner.Bytes()
		if len(l) == 0 {
			continue
		}
		_, v := parse(l)
		packets = append(packets, v)
	}

	// add in our 'marker' packets
	packets = append(packets, []any{[]any{2}}, []any{[]any{6}})

	// sort!
	slices.SortFunc(packets, func(l, r any) bool {
		return compare(l, r) < 0
	})

	// for _, r := range packets {
	// 	dump(r)
	// }

	// our result is the product of the positions of the two
	// markers (after sorting).
	result := 1
	for i, r := range packets {
		if isMarker(r) {
			result *= i + 1
		}
	}

	fmt.Println("product of marker positions: ", result)
}

func parse(l []byte) (int, any) {
	switch l[0] {
	case '[':
		n, elems := parseListElems(l[1:])
		return n + 1, elems

	default:
		// should be an integer
		sepPos := bytes.IndexAny(l, ",]")
		if sepPos == -1 {
			sepPos = len(l)
		}
		numBytes := l[:sepPos]
		num, err := strconv.Atoi(string(numBytes))
		if err != nil {
			panic(fmt.Sprintf("error parsing bytes as number %q: %s", string(numBytes), err))
		}
		return sepPos, num
	}
}

// must consume trailing ']'
func parseListElems(l []byte) (int, []any) {
	var n int
	elems := []any{}
	for {
		if l[0] == ']' {
			return n + 1, elems
		}
		m, val := parse(l)
		elems = append(elems, val)
		n += m
		l = l[m:]
		if l[0] == ',' {
			n++
			l = l[1:]
		}
	}
}

// compare returns a negative number if l < r, 0 if
// l == r, and a positive number if l > r.
func compare(l, r any) int {
	switch lv := l.(type) {
	case int:
		switch rv := r.(type) {
		case int:
			return lv - rv
		case []any:
			return compare([]any{lv}, rv)
		}
	case []any:
		switch rv := r.(type) {
		case int:
			return compare(lv, []any{rv})
		case []any:
			if len(lv) == 0 {
				return -len(rv)
			}
			if len(rv) == 0 {
				return 1
			}
			if x := compare(lv[0], rv[0]); x != 0 {
				return x
			}
			return compare(lv[1:], rv[1:])
		}
	}
	panic("unexpected type in comparison")
}

// isMarker returns true if the value is one of our
// marker-packets - [[2]] or [[6]].
func isMarker(v any) bool {
	switch v := v.(type) {
	case []any:
		if len(v) != 1 {
			return false
		}
		switch v := v[0].(type) {
		case []any:
			return len(v) == 1 && (v[0] == 2 || v[0] == 6)
		}
	}
	return false
}

func dump(v any) {
	data, err := json.Marshal(v)
	if err != nil {
		fmt.Println("error: " + err.Error())
		return
	}
	fmt.Println(string(data))
}
