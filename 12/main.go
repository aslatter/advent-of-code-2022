package main

import (
	"aoc/queue"
	"aoc/util"
	"fmt"
)

func main() {
	scanner, close := util.Input()
	defer close()

	var grid [][]byte
	for scanner.Scan() {
		orig := scanner.Bytes()
		cp := make([]byte, len(orig))
		copy(cp, orig)
		grid = append(grid, cp)
	}

	var distance [][]int
	l := len(grid[0])
	for i := 0; i < len(grid); i++ {
		distance = append(distance, make([]int, l))
	}

	// choose our 'default' distance to be larger
	// than any actual path.
	maxDistance := 10 * len(grid) * len(grid[0])

	// we will start from the exit and analyze each
	// neighbor. If the current square is "reachable"
	// from our neighbor, and our square provides a shorter
	// path to the exit than its current distance, we update
	// our neighbors shortest-known distance and add it to
	// our queue of cells to look at.
	//
	// We don't really need a queue - we could easily do this
	// in a depth-first traversal. However a breadth-first search
	// is substantially faster than depth-first due to needing
	// to analyze outlying cells less often.

	var s state
	s.d = distance
	s.g = grid

	// find entrance and exit, and also fill in
	// a default-distance for every other square
	var exitRow, exitColumn int
	for i := 0; i < len(s.g); i++ {
		row := s.g[i]
		for j := 0; j < len(row); j++ {
			switch row[j] {
			case 'E':
				exitRow, exitColumn = i, j
				s.g[i][j] = 'z'
			case 'S':
				s.g[i][j] = 'a'
				s.d[i][j] = maxDistance
			default:
				s.d[i][j] = maxDistance
			}
		}
	}

	// queue up first cells to look at
	s.analyzeNeighbors(exitRow, exitColumn)

	// work queue
	for {
		work, ok := s.q.Dequeue()
		if !ok {
			break
		}
		s.analyzeNeighbors(work.row, work.column)
	}

	// now, find the cell with elevation 'a' with the
	// shortest path to the exit
	entranceDistance := maxDistance
	for i, row := range s.d {
		for j, n := range row {
			if s.g[i][j] == 'a' && n < entranceDistance {
				entranceDistance = n
			}
		}
	}
	fmt.Println("entrance distance: ", entranceDistance)

	// dump grid
	// fmt.Println()
	// for _, row := range s.d {
	// 	for _, c := range row {
	// 		fmt.Printf(" %5d", c)
	// 	}
	// 	fmt.Println()
	// }
}

type state struct {
	// cells to score next
	q queue.Queue[workItem]
	// discovered distances so-far
	d [][]int
	// our height-grid
	g [][]byte
}

// we generate work-items by looking at the neighbors
// of a cell we know the distance from
type workItem struct {
	row    int
	column int
}

func (s *state) analyzeNeighbors(i, j int) {
	myHeight := s.g[i][j]
	myScore := s.d[i][j]

	if i > 0 {
		s.analyzeNeighbor(i-1, j, myHeight, myScore)
	}
	if j < len(s.g[0])-1 {
		s.analyzeNeighbor(i, j+1, myHeight, myScore)
	}
	if i < len(s.g)-1 {
		s.analyzeNeighbor(i+1, j, myHeight, myScore)
	}
	if j > 0 {
		s.analyzeNeighbor(i, j-1, myHeight, myScore)
	}
}

func (s *state) analyzeNeighbor(i, j int, myHeight byte, myScore int) {
	nHeight := s.g[i][j]
	if myHeight > nHeight && myHeight-nHeight > 1 {
		// not reachable from there
		return
	}
	if myScore+1 >= s.d[i][j] {
		// already has a better score - nothing to do
		return
	}
	// adjust score, queue for analysis
	s.d[i][j] = myScore + 1
	s.q.Enqueue(workItem{row: i, column: j})
}
