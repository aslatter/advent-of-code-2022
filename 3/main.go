package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	var inputPath string
	flag.StringVar(&inputPath, "input", "", "path to input file")
	flag.Parse()

	if inputPath == "" {
		fmt.Print("flag --input is required")
		os.Exit(1)
	}

	file, err := os.Open(inputPath)
	if err != nil {
		fmt.Printf("opening file %q: %s", inputPath, err)
		os.Exit(1)
	}

	var score int

	r := bufio.NewScanner(file)
	for r.Scan() {
		line1 := strings.TrimSpace(r.Text())
		if line1 == "" {
			fmt.Println("unexpected empty line reading line 1 of group")
			os.Exit(1)
		}

		if !r.Scan() {
			fmt.Println("unexpected end of input reading line 2 of group")
			os.Exit(1)
		}
		line2 := strings.TrimSpace(r.Text())
		if line2 == "" {
			fmt.Println("unexpected empty line reading line 2 of group")
			os.Exit(1)
		}

		if !r.Scan() {
			fmt.Println("unexpected end of input reading line 3 of group")
			os.Exit(1)
		}
		line3 := strings.TrimSpace(r.Text())
		if line3 == "" {
			fmt.Println("unexpected empty line reading line 3 of group")
			os.Exit(1)
		}

		seenChar1 := make(map[rune]bool)
		for _, c := range line1 {
			seenChar1[c] = true
		}

		seenChar2 := make(map[rune]bool)
		for _, c := range line2 {
			if seenChar1[c] {
				seenChar2[c] = true
			}
		}

		for _, c := range line3 {
			if seenChar2[c] {
				score += calculateScore(c)
				break
			}
		}
	}

	fmt.Println("Score: ", score)
}

func calculateScore(c rune) int {
	switch {
	case c >= 'a' && c <= 'z':
		return int(26 - ('z' - c))
	case c >= 'A' && c <= 'Z':
		return int(52 - ('Z' - c))
	}
	return 0
}
