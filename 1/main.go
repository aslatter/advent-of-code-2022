package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"golang.org/x/exp/slices"
)

type state struct {
	carrying []int
}

func main() {
	var inputPath string
	flag.StringVar(&inputPath, "input", "", "input file to read from")
	flag.Parse()

	if inputPath == "" {
		fmt.Println("flag --input is required")
		os.Exit(1)
	}

	f, err := os.Open(inputPath)
	if err != nil {
		fmt.Println("error opening input: ", err)
		os.Exit(1)
	}
	defer f.Close()

	r := bufio.NewScanner(f)
	var s state
	var current int
	for r.Scan() {
		numStr := r.Text()
		if strings.TrimSpace(numStr) == "" {
			if current > 0 {
				s.carrying = append(s.carrying, current)
				current = 0
			}
			continue
		}

		num, err := strconv.Atoi(numStr)
		if err != nil {
			fmt.Printf("error parsing number %q: %s", numStr, err)
			os.Exit(1)
		}
		current += num
	}
	if current > 0 {
		s.carrying = append(s.carrying, current)
	}

	fmt.Println("Number of elves: ", len(s.carrying))

	slices.SortFunc(s.carrying, func(l, r int) bool {
		return r < l
	})

	// sum the top three
	total := s.carrying[0] + s.carrying[1] + s.carrying[2]

	fmt.Println("The top three elves have: ", total)
}
