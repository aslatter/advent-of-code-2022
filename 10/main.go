package main

import (
	"aoc/util"
	"fmt"
)

func main() {
	sc, close := util.Input()
	defer close()

	var s state
	s.counter = 1
	s.cycle = 1
	s.draw()

	for sc.Scan() {
		str := sc.Text()
		switch {
		case str == "noop":
			s.runMicrocode(0)
		default:
			var amount int
			_, err := fmt.Sscanf(str, "addx %d", &amount)
			if err != nil {
				panic(err)
			}
			// break addx into two primitive adds, the first for zero
			s.runMicrocode(0)
			s.runMicrocode(amount)
		}
	}
}

type state struct {
	counter int
	cycle   int
}

func (s *state) runMicrocode(increment int) {
	s.counter += increment
	s.cycle++

	s.draw()
}

func (s *state) draw() {
	c := s.cycle
	hPos := (c - 1) % 40

	if hPos == 0 {
		fmt.Println()
	}

	switch hPos - s.counter {
	case -1, 0, 1:
		fmt.Print("#")
	default:
		fmt.Print(".")
	}
}
