package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var inputPath string
	flag.StringVar(&inputPath, "input", "", "path to input file")
	flag.Parse()

	if inputPath == "" {
		fmt.Print("flag --input is required")
		os.Exit(1)
	}

	file, err := os.Open(inputPath)
	if err != nil {
		fmt.Printf("opening file %q: %s", inputPath, err)
		os.Exit(1)
	}

	var count int

	r := bufio.NewScanner(file)
	for r.Scan() {
		s := strings.TrimSpace(r.Text())
		if s == "" {
			continue
		}
		p := parsePairs(s)
		if p.hasAnyOverlap() {
			count++
		}
	}
	fmt.Println("Count: ", count)
}

type assignment struct {
	start int
	end   int
}

type pair struct {
	p1 assignment
	p2 assignment
}

func parsePairs(s string) (p pair) {
	pieces := strings.SplitN(s, ",", 3)
	if len(pieces) != 2 {
		panic(fmt.Sprintf("invalid pair: %q", s))
	}
	p.p1 = parseAssignment(pieces[0])
	p.p2 = parseAssignment(pieces[1])
	return
}

func (p pair) hasAnyOverlap() bool {
	return p.p1.isInRange(p.p2.start) || p.p2.isInRange(p.p1.start)
}

func parseAssignment(s string) (a assignment) {
	pieces := strings.SplitN(s, "-", 3)
	if len(pieces) != 2 {
		panic(fmt.Sprintf("invalid assignment: %q", s))
	}
	var err error
	a.start, err = strconv.Atoi(pieces[0])
	if err != nil {
		panic(err)
	}
	a.end, err = strconv.Atoi(pieces[1])
	if err != nil {
		panic(err)
	}
	return
}

func (a assignment) isInRange(i int) bool {
	return a.start <= i && a.end >= i
}
