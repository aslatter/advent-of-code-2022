package main

import (
	"aoc/util"
	"fmt"
	"strings"

	"golang.org/x/exp/slices"
)

func main() {
	s, close := util.Input()
	defer close()

	root := &folder{}
	cwd := root
done:
	for {
		cmd := s.Text()
		switch {
		case cmd == "$ cd /":
			cwd = root
		case cmd == "$ cd ..":
			if cwd.parent != nil {
				cwd = cwd.parent
			} else {
				panic("cannot cd .. at root")
			}
		case strings.HasPrefix(cmd, "$ cd "):
			dirName := strings.TrimPrefix(cmd, "$ cd ")
			cwd = cwd.insertFolder(dirName)
		case cmd == "$ ls":
			for {
				if !s.Scan() {
					break done
				}
				entryStr := s.Text()
				if strings.HasPrefix(entryStr, "$") {
					break
				}
				if strings.HasPrefix(entryStr, "dir ") {
					continue
				}
				var f file
				var n string
				_, err := fmt.Sscanf(entryStr, "%d %s", &f.size, &n)
				if err != nil {
					panic(err)
				}
				cwd.insertFile(n, f)
			}

			continue
		}

		if !s.Scan() {
			break
		}
	}

	// sum each folder
	sums, rootSize := root.accumulateAndSum(nil)

	// find smallest folder that frees up
	// enough space.

	const totalSize = 70_000_000
	const goalFree = 30_000_000

	currentFree := totalSize - rootSize
	neededDelete := goalFree - currentFree

	if neededDelete < 1 {
		fmt.Println("no need to delete anything")
		return
	}

	found := rootSize
	for _, sz := range sums {
		if sz >= neededDelete && sz < found {
			found = sz
		}
	}

	fmt.Println("deletion size: ", found)
}

type dirEnt struct {
	name  string
	entry any
}

type file struct {
	size int
}

type folder struct {
	parent   *folder // ick
	children []dirEnt
}

// insertFolder creates (or looks up) a child folder of
// the given name.
func (f *folder) insertFolder(name string) *folder {
	// this could be simpler if we take advantage of the fact
	// we only ever visit a given folder once.
	pos, found := slices.BinarySearchFunc(f.children, dirEnt{name: name}, compareDirEnt)
	if found {
		ent := &f.children[pos]
		switch v := ent.entry.(type) {
		case *folder:
			return v
		default:
			panic("unexpected directory entry with name " + name)
		}
	}
	child := &folder{
		parent: f,
	}
	ent := dirEnt{
		name:  name,
		entry: child,
	}
	f.children = slices.Insert(f.children, pos, ent)
	return child
}

// insertFile adds a file with a given name to a folder
func (f *folder) insertFile(name string, e file) {
	// this could be simpler if we take advantage of the fact
	// we never 'ls' twice in the same folder.
	pos, found := slices.BinarySearchFunc(f.children, dirEnt{name: name}, compareDirEnt)
	if found {
		// !!??
		panic("did not expect to find dirent with name " + name)
	}
	ent := dirEnt{
		name:  name,
		entry: e,
	}
	f.children = slices.Insert(f.children, pos, ent)
}

func compareDirEnt(lhs, rhs dirEnt) int {
	switch {
	case lhs.name < rhs.name:
		return -1
	case lhs.name > rhs.name:
		return 1
	default:
		return 0
	}
}

// accumulateAndSum returns the total size of the folder, as
// well as inserting the size into 'acc'. We additionally insert
// the size of any sub-folders into 'acc'.
func (f *folder) accumulateAndSum(acc []int) ([]int, int) {
	var localTotal int
	for _, ent := range f.children {
		switch child := ent.entry.(type) {
		case file:
			localTotal += child.size
		case *folder:
			var folderSize int
			acc, folderSize = child.accumulateAndSum(acc)
			localTotal += folderSize
		}
	}
	return append(acc, localTotal), localTotal
}
