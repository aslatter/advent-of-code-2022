package main

import (
	"aoc/util"
	"fmt"
)

const period = 14

func main() {
	s, close := util.Input()
	defer close()

	var result int

	s.Scan()
	line := s.Bytes()

	// last few symbols seen
	var r [period]byte

	// first fill our register of seen elements
	for j := 0; j < period; j++ {
		r[j] = line[j]
	}

	if elemsUnique(r) {
		result = period
	} else {
		for j := period; j < len(line); j++ {
			r[j%period] = line[j]
			if elemsUnique(r) {
				result = j + 1
				break
			}
		}
	}

	fmt.Println("result: ", result)
}

func elemsUnique(r [period]byte) bool {
	var seen [256]bool
	for _, b := range r {
		if seen[b] {
			return false
		}
		seen[b] = true
	}
	return true
}
